import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
{
title: 'Dashboard',
icon: 'nb-home',
link: '/pages/dashboard',
home: true,
},
{
title: 'MENU',
group: true,
},{
    title: 'Subscription',
    icon : 'nb-layout-default',
    link: '/pages/subscription',
    },
{
title: 'Organizational Unit',
icon : 'nb-keypad',
link: '/pages/organization',
},
{
title: 'Projects',
link: '/pages/projects',
icon : 'nb-list',
},
{
title: 'Billing Information',
icon : 'nb-compose',
children: [
{
title: 'AWS Billing',
link: 'pages/tables/smart-table',
},
{
title: 'Google Billing',
link: 'pages/tables1/smart-table',
},
],

},

{
title: 'EA Management',
icon : 'nb-bar-chart',
children: [
{
title: 'Azure EA',
link: 'pages/tables/smart-table',
},
{
title: 'AWS EA',
link: 'pages/tables1/smart-table',
},
{
title: 'Goolge Cloud EA',
link: 'pages/tables1/smart-table',
},
],

},

{
title: 'Quota Allocation',
icon : 'nb-drops',
children: [
{
title: 'Azure EA',
link: 'pages/tables/smart-table',
},
{
title: 'AWS EA',
link: 'pages/tables1/smart-table',
},
{
title: 'Goolge Cloud EA',
link: 'pages/tables1/smart-table',
},
],

},

{
title: 'Auth',
icon: 'nb-locked',
children: [
{
title: 'Login',
link: '/auth/login',
},
{
title: 'Register',
link: '/auth/register',
},
{
title: 'Request Password',
link: '/auth/request-password',
},
{
title: 'Reset Password',
link: '/auth/reset-password',
},
],
},
];
