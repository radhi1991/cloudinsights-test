import { NgModule } from '@angular/core';


import { ThemeModule } from '../../@theme/theme.module';
import { OrganizationComponent } from './organization.component';
import {MatSliderModule} from '@angular/material/slider';

@NgModule({
  imports: [
    ThemeModule,MatSliderModule
  ],
  declarations: [
    OrganizationComponent,
  ],
})
export class OrganizationModule { }
