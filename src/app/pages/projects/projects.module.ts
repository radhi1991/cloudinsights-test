import { NgModule } from '@angular/core';


import { ThemeModule } from '../../@theme/theme.module';
import { ProjectsComponent } from './projects.component';
import {MatSliderModule} from '@angular/material/slider';

@NgModule({
  imports: [
    ThemeModule,MatSliderModule
  ],
  declarations: [
    ProjectsComponent,
  ],
})
export class ProjectsModule { }
